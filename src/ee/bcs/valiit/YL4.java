package ee.bcs.valiit;

public class YL4 {
    static void leiaSajand(int aasta) {
        if (aasta < 1 || aasta > 2018)
            System.out.print("Kammoon noh???\n");

        else if (aasta <= 100)
            System.out.print("Esimene sajand\n");

        else if (aasta % 100 == 0)
            System.out.print(aasta / 100 + " sajand\n");

        else
            System.out.print(aasta / 100 + 1 + " sajand\n");

        int[] numbers = new int[]{0, 1, 128, 598, 1624, 1827, 1996, 2017};
        for (int i = 0; i < numbers.length; i++) {
            leiaSajand(numbers[i]);
        }

    }
}
