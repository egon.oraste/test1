package ee.bcs.valiit;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class YL5 {

    public static void main(String[] args) {

        List<Country> countries = createList();

        List<Country> sorted_countries = countries.stream()
                .sorted(Comparator.comparingInt(Country::getPopulation)).collect(Collectors.toList());

        System.out.println(sorted_countries);
    }

    private static List<Country> createList() {

        List<Country> countries = new ArrayList<>();

        countries.add(new Country("Slovakia", 5424000));
        countries.add(new Country("Hungary", 9845000));
        countries.add(new Country("Poland", 38485000));
        countries.add(new Country("Germany", 81084000));
        countries.add(new Country("Latvia", 1978000));

        return countries;
    }
}